<?php
	
	namespace Redirect;

	class Redirect {
		
		public static function go( $location, $code=301, $message=null ) {
			if( $code == 301 && $message===null )
				$message = 'Moved Permanently';

			if( $code == 302 && $message===null )
				$message = 'Moved Temporarily';

			header('HTTP/1.1 ' . $code . ' ' . $message );
			header('Location:'.$location);
			header('Refresh:0;url=' . $location );
			?>
			<html>
				<head>
					<meta http-equiv="refresh" content="0;url=<?php echo $location;?>" />
					<meta name="robots" content="noindex" />
					<title>U wordt doorverwezen naar <?=$location?>, een ogenblik geduld</title>
				</head>
				<body>
					<script type="text/javascript">
						document.location = "<?php echo $location; ?>";
					</script>
					<h1>U wordt doorverwezen</h1>
					<p>
						Indien er niets gebeurd kan u zelf <a href="<?=$location?>">verderklikken</a>.
					</p>
				</body>
			</html>
			<?php
			exit;
		}
		
	}